package com.tireaudit.tirescan;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.media.MediaPlayer;
import android.media.MediaScannerConnection;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Environment;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.text.method.Touch;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class CameraPreview
		implements
			SurfaceHolder.Callback, Camera.PreviewCallback {
	
	public Camera mCamera;
	public Camera.Parameters params;
	private SurfaceHolder sHolder;
    public uploadPictures upPic;

	public List<Camera.Size> supportedSizes;

	public int isCamOpen = 0;
	public boolean isSizeSupported = false;
	public boolean useFocused = false;
	public int previewWidth, previewHeight, picWidth, picHeight;

	private Boolean TakePicture = false;

	public String NowPictureFileName;
	private int mTimeLag = 100;
	public int mNumMaxPics = 3;
	public int mPicId = 0;
    public int mCount = 0;

    public int screenWidth, screenHeight;

    public Rect focusRect = new Rect(0, 0, 0, 0);

    public Vibrator vibrator;

	private final static String TAG = "CameraPreview";

    android.os.Handler mHandler = new android.os.Handler();

    //private TouchActivity touchActivity = new TouchActivity();
	private PreviewSurfaceView camView;

    public ArrayList<String> toBeScanned;

    private Rect focusRect1, focusRect2, focusRect3;

    private byte[][] images;

    private int actualPicCount = 0;

    private Context context;

	public CameraPreview(Context context, int width, int height, int resWidth, int resHeight) {
		Log.i(TAG, "Preview Width = " + String.valueOf(width));
		Log.i(TAG, "Preview Height = " + String.valueOf(height));
		previewWidth = width;
		previewHeight = height;

        Log.i(TAG, "Preview Width = " + String.valueOf(resWidth));
        Log.i(TAG, "Preview Height = " + String.valueOf(resHeight));
        picWidth = resWidth;
        picHeight = resHeight;

        toBeScanned = new ArrayList<String>();

        this.context = context;

        upPic = new uploadPictures(context);

        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	}

    public List<Camera.Size> getResolutionList() {
        return mCamera.getParameters().getSupportedPictureSizes();    //getSupportedPreviewSizes();
    }

    public Camera.Size getResolution() {

        return mCamera.getParameters().getPictureSize();
    }

    public void setResolution(Camera.Size resolution) {
        //disconnectCamera();
        //previewHeight = resolution.height;
        //previewWidth = resolution.width;
        //connectCamera(getWidth(), getHeight());
        picWidth = resolution.width;
        picHeight = resolution.height;
        params.setPictureSize(picWidth, picHeight);
        mCamera.setParameters(params);
    }

    public void setPreviewResolution(Camera.Size resolution) {

        previewHeight = resolution.height;
        previewWidth = resolution.width;

        params.setPreviewSize(previewWidth, previewHeight);
        mCamera.setParameters(params);
    }


    public void setPicFormat(Integer picFormat){

        params.setPictureFormat(picFormat);
        mCamera.setParameters(params);

    }


    @Override
    public void onPreviewFrame(byte[] arg0, Camera arg1)
    {
        // At preview mode, the frame data will push to here.
        // But we do not want these data.
    }

	public int isCamOpen() {
		return isCamOpen;
	}

	public void releaseCamera() {

		Log.i(TAG, "CAMERA RELEASE CALLED");

		if (mCamera != null) {
			mCamera.stopPreview();
			mCamera.setPreviewCallback(null);
			mCamera.release();
			mCamera = null;
		}
		isCamOpen = 0;
		Log.i(TAG, "CAMERA RELEASED");
	}



    @Override
    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3)
    {
        //Camera.Parameters parameters = mCamera.getParameters();
        // Set the camera preview size
        params.setPreviewSize(previewWidth, previewHeight);
        // Set the take picture size, you can set the large size of the camera supported.
        params.setPictureSize(picWidth, picHeight);

        mCamera.setParameters(params);

        mCamera.startPreview();
    }

    @Override
    public void surfaceCreated(SurfaceHolder arg0)
    {
        Log.i(TAG, "Surface Created");

        if(mCamera== null) {
            Log.e(TAG, "Camera is null");
            mCamera = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK);

            params = mCamera.getParameters();
            params.setPreviewSize(previewWidth, previewHeight);
            params.setPictureSize(picWidth, picHeight);
            mCamera.setParameters(params);
            //params.setAutoExposureLock(true);
        }


        try {
            mCamera.setParameters(params);
        } catch (RuntimeException e) {
            Log.e(TAG, "Camera parameters not set");
            e.printStackTrace();
        }

        mCamera.startPreview();

        try
        {
            // If did not set the SurfaceHolder, the preview area will be black.
            mCamera.setPreviewDisplay(arg0);
            mCamera.setPreviewCallback(this);
        }
        catch (IOException e)
        {
            mCamera.release();
            mCamera = null;
        }
    }



	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {

		Log.i(TAG, "SURFACE DESTROYED");

        //camView.scanImageFiles(toBeScanned);

		releaseCamera();

	}

	/**
	 * Called from PreviewSurfaceView to set touch focus.
	 * 
	 * @param - Rect - new area for auto focus
	 */
	public void doTouchFocus(final Rect tfocusRect) {
		Log.i(TAG, "TouchFocus");

        useFocused = false;

		try {
			final List<Camera.Area> focusList = new ArrayList<Camera.Area>();
			Camera.Area focusArea = new Camera.Area(tfocusRect, 1000);
			focusList.add(focusArea);
		  
			Camera.Parameters para = mCamera.getParameters();
			para.setFocusAreas(focusList);
			para.setMeteringAreas(focusList);
			mCamera.setParameters(para);
		  
			mCamera.autoFocus(myAutoFocusCallback);
		} catch (Exception e) {
			e.printStackTrace();
			Log.i(TAG, "Unable to autofocus");
            //doTouchFocus(tfocusRect);
		}

	}
	
	/**
	 * AutoFocus callback
	 */
	AutoFocusCallback myAutoFocusCallback = new AutoFocusCallback(){

		  @Override
		  public void onAutoFocus(boolean arg0, Camera arg1) {
		   if (arg0){
		    //mCamera.cancelAutoFocus();
               //isFocused = true;
               //mPicId++;
               //if(mPicId == 0)
                //mCamera.takePicture(shutterCallback, null, jpegPictureCallback);
		   }
	    }
	};

    public void lockFocusExposurePlatformMode(Rect rect){

        try {
            mCamera.cancelAutoFocus();
            final List<Camera.Area> focusList = new ArrayList<Camera.Area>();
            Camera.Area focusArea = new Camera.Area(rect, 1000);
            focusList.add(focusArea);

            Camera.Parameters para = mCamera.getParameters();
            para.setFocusAreas(focusList);
            para.setMeteringAreas(focusList);
            para.setAutoExposureLock(true);
            mCamera.setParameters(para);

            mCamera.autoFocus(new AutoFocusCallback() {
                @Override
                public void onAutoFocus(boolean success, Camera camera) {
                    if (success) {
                        Log.i(TAG, "AutoFocus completed");

                        vibrator = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);

                        //start vibration
                        vibrator.vibrate(2000);

                    }
                }
            });
        }

        catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "Unable to autofocus");
        }


    }


    public void setFocusRect(Rect rect1, Rect rect2, Rect rect3){

        focusRect1 = new Rect(rect1);
        focusRect2 = new Rect(rect2);
        focusRect3 = new Rect(rect3);

    }

	// Take picture interface
	public void CameraTakePicture(String FileName, int maxNumPics, int lagtime)
	{
        //mCamera.cancelAutoFocus();
        mNumMaxPics = maxNumPics;
        mTimeLag = lagtime;
        images = new byte[maxNumPics][];
		TakePicture = false;
		NowPictureFileName = FileName;
		mCamera.setPreviewCallback(null);
        mHandler.postDelayed(runTakePicture(), mTimeLag);

        //String[] path_split = NowPictureFileName.split("/");
        //upPic.ftpDir = path_split[path_split.length-2];
	}


	// Take picture callback

	Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback()
	{
		public void onShutter()
		{
			// Just do nothing.
		}
	};

	Camera.PictureCallback rawPictureCallback = new Camera.PictureCallback()
	{
		public void onPictureTaken(byte[] arg0, Camera arg1)
		{
			// Just do nothing.
		}
	};


    public Runnable runTakePicture() {
        return new Runnable() {
            public void run() {
                if(useFocused) {
                    try {
                        mCamera.cancelAutoFocus();
                        final List<Camera.Area> focusList = new ArrayList<Camera.Area>();
                        Camera.Area focusArea = new Camera.Area(focusRect1, 1000);
                        if (mCount == 1)
                            focusArea = new Camera.Area(focusRect2, 1000);
                        if (mCount == 2)
                            focusArea = new Camera.Area(focusRect3, 1000);
                        if (mCount == 3)
                            mCount = 0;

                        focusList.add(focusArea);

                        Camera.Parameters para = mCamera.getParameters();
                        para.setFocusAreas(focusList);
                        para.setMeteringAreas(focusList);
                        mCamera.setParameters(para);

                        mCamera.autoFocus(new AutoFocusCallback() {
                            @Override
                            public void onAutoFocus(boolean success, Camera camera) {
                                if (success)
                                    camera.takePicture(shutterCallback, null, jpegPictureCallback);
                            }
                        });
                    }

                    catch (Exception e) {
                        e.printStackTrace();
                        Log.i(TAG, "Unable to autofocus");
                        //doTouchFocus(tfocusRect);
                    }
                }
                else
                    mCamera.takePicture(shutterCallback, null, jpegPictureCallback);

            }
        };
    }

	Camera.PictureCallback jpegPictureCallback = new Camera.PictureCallback()
	{
		public void onPictureTaken(byte[] data, Camera arg1)
		{

            try {

                    images[mPicId] = new byte[1];
                    images[mPicId] = Arrays.copyOf(data, data.length);

            }

            catch (Exception e) {
                    Log.e("PictureDemo", "Exception in photoCallback", e);
                    e.printStackTrace();

            }

            mPicId++;
            mCount++;
            actualPicCount++;

            if (mPicId < mNumMaxPics) {
                Log.i(TAG, "mPicID: " + mPicId + " of " + mNumMaxPics);
                mCamera.startPreview();
                mHandler.postDelayed(runTakePicture(), mTimeLag);
            }

            else {
                Log.i(TAG, "Multiple image capture finished");
                Log.i(TAG, "Total no of pics captured: " + actualPicCount);

                if(actualPicCount < mPicId)
                    actualPicCount--;

                String picName = "";

                for (int i = 0; i < actualPicCount; i++) {
                    try {
                        if( i < 10)
                            picName = NowPictureFileName + "_000" + i + ".jpg";
                        else if (i > 10 && i < 100)
                            picName = NowPictureFileName + "_00" + i + ".jpg";
                        else if (i > 100 && i < 1000)
                            picName = NowPictureFileName + "_0" + i + ".jpg";
                        else if (i > 1000 && i < 10000)
                            picName = NowPictureFileName + "_" + i + ".jpg";

                        FileOutputStream out = new FileOutputStream(picName);

                        out.write(images[i]);
                        out.close();

                        toBeScanned.add(picName);
                        upPic.imageToSend = picName;
                    }
                    catch(IOException e){
                        Log.e("PictureDemo", "Exception in photoCallback", e);
                        e.printStackTrace();
                    }


                }

                mPicId = 0;
                actualPicCount = 0;
                useFocused = false;
                mCamera.startPreview();

                for(int i = 0; i < toBeScanned.size(); i++){

                    File f = new File(toBeScanned.get(i));

                    MediaScan ms = new MediaScan(context, f);

                }

                //upPic.execute();

                String[] path_split = NowPictureFileName.split("/");

                new AlertDialog.Builder(context)
                        .setTitle("Scan Complete")
                        .setMessage("Images are saved in folder: " + path_split[path_split.length - 2])
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with upload to ftp
                                upPic.execute();
                            }
                        }).show();

            }

        }

	};


    //Method for uploading picture on FtpServer

}