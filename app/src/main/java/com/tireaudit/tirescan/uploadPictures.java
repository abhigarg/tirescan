package com.tireaudit.tirescan;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

//import org.jibble.simpleftp.SimpleFTP;

import java.io.File;


/**
 * Created by ABGG on 12/11/2015.
 */
public class uploadPictures extends AsyncTask<String, Integer, String> {

    private String TAG = "ftpActivity";
    public String imageToSend = ""; //Environment.getExternalStorageDirectory().getPath() + "Tulips.jpg"
    public String ftpDir = "";

    public SimpleFTP ftp = new SimpleFTP();

    private Context context;

    public uploadPictures(Context context){

        this.context = context;
    }

    @Override
    protected String doInBackground(String... params) {
        // ftpClient=uploadingFilestoFtp();
        try {

            ftp.connect("abgg.ddns.net", 21,"abgg","abc");

            ftp.bin();

            // Change to a new working directory on the FTP server.
            ftp.cwd("test");

            // Upload some files.
            if(imageToSend.length() > 1)
                ftp.stor(new File(imageToSend));

            Log.i(TAG, "Files uploaded successfully");

            // ftp.stor(new File("comicbot-latest.png"));

            // You can also upload from an InputStream, e.g.
            // ftp.stor(new FileInputStream(new File("test.png")),
            // "test.png");
            // ftp.stor(someSocket.getInputStream(), "blah.dat");

            // Quit from the FTP server.
            //ftp.disconnect();

        } catch (Exception e) {
            Log.e(TAG, "Could not connect or send file to ftp");
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // dialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        //Toast.makeText(context, "sent", Toast.LENGTH_LONG).show();
        Log.i(TAG, "Sent: " + imageToSend);
        /*try {
            ftp.disconnect();
        }catch (Exception e) {
            Log.e(TAG, "Could not disconnect from ftp server");
            e.printStackTrace();
        }*/
    }
}

