package com.tireaudit.tirescan;


import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.util.Size;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import org.jibble.simpleftp.SimpleFTP;
import org.apache.commons.net.ftp.FTPClient;
import android.os.AsyncTask;



@SuppressWarnings("deprecation")
public class TouchActivity extends Activity {

    private String TAG = "TouchActivity";
	private PreviewSurfaceView camView;
	private CameraPreview cameraPreview;
	private DrawingView drawView;
	
	private int previewWidth = 1280; //3264;1440;1280;1280;2592
	private int previewHeight = 720; //2448;1080;720;960;1944

    private int resWidth = 3264;
    private int resHeight = 2448;

	private String extStorageDirectory = Environment.getExternalStorageDirectory().getPath();
    private String configFilePath = extStorageDirectory + "/config.txt";
	public String PictureFileName = extStorageDirectory + "/DCIM/TireScan/";

    //Menu
    private List<Camera.Size> mResolutionList;
    private MenuItem[] mResolutionMenuItems;
    private SubMenu mResolutionMenu;

    private List<Camera.Size> mPreviewResolutionList;
    private MenuItem[] mPreviewResolutionMenuItems;
    private SubMenu mPreviewResolutionMenu;

    private MenuItem[] mMultiplePicItems;
    private SubMenu mMultiplePics;


    private MenuItem[] mCameraViewParamItems;
    private SubMenu mCameraViewParam;

    private int maxNumPics;
    private int lagTime;
    private int rectX;
    private int rectY = 300;
    private DrawingView drawingView;

    private static Context context;

    public Rect targetFocusRect1 = new Rect(0, 0, 0, 0);
    public Rect targetFocusRect2 = new Rect(0, 0, 0, 0);
    public Rect targetFocusRect3 = new Rect(0, 0, 0, 0);

    public int rectX1 = 100;
    public int rectX2 = 600;
    public int rectX3 = 1000;

    public int rectY1 = 300;
    public int rectY2 = 300;
    public int rectY3 = 300;

    public Rect screenRect1 = new Rect(
            (int)(rectX1 - 100),
            (int)(rectY1 - 100),
            (int)(rectX1 + 100),
            (int)(rectY1 + 100));

    public Rect screenRect2 = new Rect(
            (int)(rectX2 - 100),
            (int)(rectY2 - 100),
            (int)(rectX2 + 100),
            (int)(rectY2 + 100));

    public Rect screenRect3 = new Rect(
            (int)(rectX3 - 100),
            (int)(rectY3 - 100),
            (int)(rectX3 + 100),
            (int)(rectY3 + 100));

    public Rect fRect1, fRect2, fRect3;


    //Scan modes
    boolean freeStandingScanMode = false;
    boolean platformScanMode = false;

    //FTP client
    FTPClient ftpClient;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

		setContentView(R.layout.activity_touch);
		
		camView = (PreviewSurfaceView) findViewById(R.id.preview_surface);
        camView.setVisibility(SurfaceView.VISIBLE);
		SurfaceHolder camHolder = camView.getHolder();

        String MyDirectory_path = extStorageDirectory;

        File file = new File(MyDirectory_path);
        if (!file.exists()) {
            file.mkdirs();
            Toast.makeText(getBaseContext(), "Path not found. Creating new directory", Toast.LENGTH_SHORT).show();
        }

        File configFile = new File(configFilePath);

        if(!configFile.exists()){
            Toast.makeText(getBaseContext(), "Config file not found. Create a config file", Toast.LENGTH_LONG).show();
        }
        else if(configFile.exists()){
            Log.i(TAG, "Reading values from configuration file");
        }

        //Read from maxNumPics and PictureResolution from config file
        StringBuilder text = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(configFile));
            String line;

            while ((line = br.readLine()) != null) {
                text.append(line);
                text.append('\n');
                Log.i(TAG, line);
                String[] separated = line.split(";");
                 if(separated[0].equals("maxNumPics")){
                     maxNumPics = Integer.parseInt(separated[1]);
                     Log.i(TAG, "Max. no of pics set to " + Integer.valueOf(maxNumPics).toString());
                     Toast.makeText(getBaseContext(), "Max. no of pics set to " + Integer.valueOf(maxNumPics).toString(),
                                    Toast.LENGTH_SHORT).show();
                 }

                else if(separated[0].equals("picResolution")){
                     Log.i(TAG, separated[1]);
                     String[] picResolution = separated[1].split("x");
                     Log.i(TAG, "picResolution: w = " + picResolution[0] + ", h = " + picResolution[1]);

                     resWidth = Integer.parseInt(picResolution[0]);
                     resHeight = Integer.parseInt(picResolution[1]);

                 }

            }
            br.close();
            Toast.makeText(getBaseContext(), text, Toast.LENGTH_LONG).show();
        }
        catch (IOException e) {
            Log.e(TAG, "Error reading config file. Using raw default values");

            maxNumPics = 10; //in numbers
            //You'll need to add proper error handling here
        }

        cameraPreview = new CameraPreview(this, previewWidth, previewHeight, resWidth, resHeight);

        camHolder.addCallback(cameraPreview);
        camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        camView.setListener(cameraPreview);
        //cameraPreview.changeExposureComp(-currentAlphaAngle);
        drawingView = (DrawingView) findViewById(R.id.drawing_surface);
        camView.setDrawingView(drawingView);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        lagTime = 100;  //in milisec

	}

    public void resetScreenRect(){

        rectX1 = camView.x1;
        rectX2 = camView.x2;
        rectX3 = camView.x3;

        rectY1 = camView.y1;
        rectY2 = camView.y2;
        rectY3 = camView.y3;

        screenRect1 = new Rect(
                (int)(rectX1 - 100),
                (int)(rectY1 - 100),
                (int)(rectX1 + 100),
                (int)(rectY1 + 100));

        screenRect2 = new Rect(
                (int)(rectX2 - 100),
                (int)(rectY2 - 100),
                (int)(rectX2 + 100),
                (int)(rectY2 + 100));


        screenRect3 = new Rect(
                (int)(rectX3 - 100),
                (int)(rectY3 - 100),
                (int)(rectX3 + 100),
                (int)(rectY3 + 100));

        camView.isTouched = false;

    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

        Log.e(TAG, "Menu Created");

        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.touch, menu);


        //Camera picture resolution
        mResolutionMenu = menu.addSubMenu("Camera Picture Resolution");
        mResolutionList = cameraPreview.getResolutionList();
        mResolutionMenuItems = new MenuItem[mResolutionList.size()];

        ListIterator<Camera.Size> resolutionItr = mResolutionList.listIterator();
        int idx = 0;
        while (resolutionItr.hasNext()) {
            Camera.Size element = resolutionItr.next();
            mResolutionMenuItems[idx] = mResolutionMenu.add(1, idx, Menu.NONE,
                    Integer.valueOf(element.width).toString() + "x" + Integer.valueOf(element.height).toString());
            idx++;
        }

        // Multiple pics
        mMultiplePics = menu.addSubMenu("Multiple Pics");
        mMultiplePicItems = new MenuItem[2];

        mMultiplePicItems[0] = mMultiplePics.add(2,0, Menu.NONE, "MaxNumPics");
        mMultiplePicItems[1] = mMultiplePics.add(2, 1, Menu.NONE, "Time Lag");

        //Show camera settings
        mCameraViewParam = menu.addSubMenu("Miscellaneous");
        mCameraViewParamItems = new MenuItem[5];
        mCameraViewParamItems[0] = mCameraViewParam.add(3, 0, Menu.NONE, "Show Current Settings");
        mCameraViewParamItems[1] = mCameraViewParam.add(3, 1, Menu.NONE, "Enter Coordinates");
        mCameraViewParamItems[2] = mCameraViewParam.add(3, 2, Menu.NONE, "FreeStanding Scan");
        mCameraViewParamItems[3] = mCameraViewParam.add(3, 3, Menu.NONE, "Platform Scan");
        mCameraViewParamItems[4] = mCameraViewParam.add(3, 4, Menu.NONE, "Scan Media");


        //camera picture format
        mPreviewResolutionMenu = menu.addSubMenu("Camera Preview Resolution");
        mPreviewResolutionList = cameraPreview.params.getSupportedPreviewSizes();
        mPreviewResolutionMenuItems = new MenuItem[mPreviewResolutionList.size()];

        ListIterator<Camera.Size> previewSizeItr = mPreviewResolutionList.listIterator();
        idx = 0;
        while (previewSizeItr.hasNext()) {
            Camera.Size element = previewSizeItr.next();
            mPreviewResolutionMenuItems[idx] = mPreviewResolutionMenu.add(4, idx, Menu.NONE,
                    Integer.valueOf(element.width).toString() + "x" + Integer.valueOf(element.height).toString());
            idx++;
        }

		return true;
	}

    @SuppressLint("SimpleDateFormat")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

        Log.i(TAG, "Menu option selected");

		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}

        else if (item.getGroupId() == 1)
        {

            id = item.getItemId();
            Camera.Size resolution = mResolutionList.get(id);
            cameraPreview.setResolution(resolution);
            resolution = cameraPreview.getResolution();
            String caption = Integer.valueOf(resolution.width).toString() + "x" + Integer.valueOf(resolution.height).toString();
            Toast.makeText(this, caption, Toast.LENGTH_SHORT).show();

        }

        //menu option for user input
        else if(item.getGroupId() == 2)
        {
            if(item.getItemId()==0) {
                showInputDialog0();

            }
            else if(item.getItemId()==1) {
                showInputDialog1();
            }
        }

        else if(item.getGroupId()==3) {
            if (item.getItemId() == 0)
                showCameraParam();
            else if (item.getItemId() == 1)
                showInputDialog2();
            else if (item.getItemId() == 2) {
                //focus on point on left side and call takePicture
                //free standing mode

                freeStandingScanMode = true;
                platformScanMode = false;

                scanInFreeStandingMode();

                new AlertDialog.Builder(this)
                        .setTitle("Free Standing Scan Mode Activated")
                        .setMessage("Press Vol. Up Button to Start Scan")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        }).show();

            }

            else if (item.getItemId() == 3) {

                platformScanMode = true;
                freeStandingScanMode = false;

                new AlertDialog.Builder(this)
                        .setTitle("Platform Scan Mode Activated")
                        .setMessage("First long press vol. up button to lock AE/AF then short press vol. up Button to Start Scan")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                            }
                        }).show();

            }

            else if (item.getItemId() == 4) {
                scanForMedia();
            }

        }

        else if(item.getGroupId()==4)
        {
            id = item.getItemId();
            Camera.Size resolution = mPreviewResolutionList.get(id);
            cameraPreview.setPreviewResolution(resolution);
            resolution = cameraPreview.params.getPreviewSize();
            String caption = Integer.valueOf(resolution.width).toString() + "x" + Integer.valueOf(resolution.height).toString();
            Toast.makeText(this, caption, Toast.LENGTH_SHORT).show();

        }

		return super.onOptionsItemSelected(item);
	}


    protected void showInputDialog0() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(TouchActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TouchActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //resultText.setText("Hello, " + editText.getText());
                        Toast.makeText(TouchActivity.this, "Max no of pics: " + editText.getText(),Toast.LENGTH_SHORT).show();
                        maxNumPics = Integer.parseInt(editText.getText().toString()); //editText.getText();
                        cameraPreview.mNumMaxPics = maxNumPics;
                        //Intent intent = new Intent(MainActivity.this, Tutorial3Activity.class);
                        //MainActivity.this.startActivity(intent);
                        //Tutorial3Activity();
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    protected void showInputDialog1() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(TouchActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog1, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TouchActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(TouchActivity.this, "Time lag: " + editText.getText() + " ms",Toast.LENGTH_SHORT).show();
                        lagTime = Integer.parseInt(editText.getText().toString());
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    protected void showInputDialog2() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(TouchActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog2, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(TouchActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(TouchActivity.this, "X, Y: (" + editText.getText() + ")",Toast.LENGTH_SHORT).show();
                        String[] separated = editText.getText().toString().split(",");
                        rectX = Integer.parseInt(separated[0]);
                        rectY = Integer.parseInt(separated[1]);

                        Log.i(TAG, "Screen coordinates at: (" + rectX + ", " + rectY + ")");
                        camView.drawRect(rectX, rectY);


                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    public void showCameraParam(){

        Camera.Parameters parameters = null;

        try {
            parameters = cameraPreview.mCamera.getParameters();
        } catch (Exception e) {
            Log.e("CameraParameters", "Exception in camera parameters", e);
        }

        if(parameters != null){

            Toast.makeText(this, "Focal length: " + parameters.getFocalLength() + "\nExposure Compensation: " +
                    parameters.getExposureCompensation() + "\nFocus mode: " + parameters.getFocusMode() +
                    "\nPicture format: " + parameters.getPictureFormat() + "\nPicture size: " +
                    parameters.getPictureSize().width + "x" + parameters.getPictureSize().height
                    , Toast.LENGTH_LONG).show();

        }
        else
            Toast.makeText(this, "Null Camera Parameters", Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        int action = event.getAction();
        int keyCode = event.getKeyCode();
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                if (action == KeyEvent.ACTION_UP) {
                    //long click vol. up button
                    if (event.getEventTime() - event.getDownTime() > ViewConfiguration.getLongPressTimeout()) {
                        //TODO long click action
                        //call Platform scan

                        Log.i(TAG, "vol up long press event");

                        if(platformScanMode)
                            scanInPlatformMode();

                    } else {
                        //TODO click action
                        //if in free standing mode call free standing mode scan
                        // call take pictures in burst mode

                       // if(platformScanMode)
                        //    cameraPreview.vibrator.cancel();

                        if(!freeStandingScanMode && !platformScanMode)
                            cameraPreview.useFocused = false;

                        //Take pictures in burst mode
                        Log.i(TAG, "vol up short press event");
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
                        String currentDateandTime = sdf.format(new Date());

                        cameraPreview.NowPictureFileName = PictureFileName + currentDateandTime +"/IMG";

                        File file = new File(PictureFileName + currentDateandTime);
                        if (!file.exists()) {
                            file.mkdirs();
                        }

                        cameraPreview.CameraTakePicture(PictureFileName + currentDateandTime + "/IMG", maxNumPics, lagTime);


                    }
                }
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                if (action == KeyEvent.ACTION_UP) {
                    if (event.getEventTime() - event.getDownTime() > ViewConfiguration.getLongPressTimeout()) {
                        //TODO long click action
                    } else {
                        //TODO click action
                        cameraPreview.mPicId = maxNumPics+1;

                        Toast.makeText(this, "Stop scanning", Toast.LENGTH_SHORT).show();

                    }
                }
                return true;
            default:
                return super.dispatchKeyEvent(event);
        }
    }


    private void scanInFreeStandingMode(){

        if(camView.isTouched)
            resetScreenRect();

        camView.getRect(screenRect1);
        fRect1 = new Rect(camView.targetFocusRect);

        camView.getRect(screenRect2);
        fRect2 = new Rect(camView.targetFocusRect);

        camView.getRect(screenRect3);
        fRect3 = new Rect(camView.targetFocusRect);

        cameraPreview.setFocusRect(fRect1, fRect2, fRect3);
        cameraPreview.useFocused = true;

    }

    private void scanInPlatformMode(){
        int rectX = 600;
        int rectY = 300;

        int sz = 100;

        Rect rect = new Rect(
                (int)(rectX - sz),
                (int)(rectY - sz),
                (int)(rectX + sz),
                (int)(rectY + sz));

        camView.getRect(rect);
        Rect fRect = new Rect(camView.targetFocusRect);

        cameraPreview.lockFocusExposurePlatformMode(fRect);

        cameraPreview.useFocused = false;

    }

    private void scanForMedia(){

        for(int i = 0; i < cameraPreview.toBeScanned.size(); i++){

            File f = new File(cameraPreview.toBeScanned.get(i));

            MediaScan ms = new MediaScan(this, f);

        }
        Toast.makeText(this, "Media Scan completed for " + cameraPreview.toBeScanned.size() + " pics", Toast.LENGTH_SHORT).show();

    }


    /*
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        int action = event.getAction();
        int key = event.getKeyCode();

        if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)){

            //cameraPreview.toBeScanned.clear();

            if(freeStandingScanMode){
                scanInFreeStandingMode();
            }

            if(platformScanMode){
                scanInPlatformMode();
            }

            if(!freeStandingScanMode && !platformScanMode)
                cameraPreview.useFocused = false;

            //Take pictures in burst mode
            Log.i(TAG, "onKey event");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
            String currentDateandTime = sdf.format(new Date());

            cameraPreview.NowPictureFileName = PictureFileName + "/" + currentDateandTime +"/Pic_";

            File file = new File(PictureFileName + "/" + currentDateandTime);
            if (!file.exists()) {
                file.mkdirs();
            }

            cameraPreview.CameraTakePicture(PictureFileName + "/" + currentDateandTime + "/Pic_", maxNumPics, lagTime);

        }

        //Stop taking anymore pictures in current sweep mode. Run media scanner for pics taken already
        if(keyCode == KeyEvent.KEYCODE_VOLUME_DOWN){

            cameraPreview.mPicId = maxNumPics+1;

            Toast.makeText(this, "Stop scanning", Toast.LENGTH_SHORT).show();

        }

        return true;
    }
*/




}
