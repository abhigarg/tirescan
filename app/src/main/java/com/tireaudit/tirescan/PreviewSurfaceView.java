package com.tireaudit.tirescan;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * SurfaceView to show LenxCameraPreview2 feed
 */
public class PreviewSurfaceView extends SurfaceView {
	
	private CameraPreview camPreview;
	private boolean listenerSet = false;
	public Paint paint;
	private DrawingView drawingView;
	private boolean drawingViewSet = false;
	public int x1 = 100;
    public int y1 = 300;
    public int x2 = 600;
    public int y2 = 300;
    public int x3 = 1000;
    public int y3 = 300;
    private int touchCount = 0;

    public boolean isTouched = false;

    //100,300 ... 600,300 ... 1100,300
    public Rect targetFocusRect = new Rect(0, 0, 0, 0);


    private String TAG = "PreviewSurface";

    private TouchActivity touchActivity = new TouchActivity();
	
	public PreviewSurfaceView(Context context, AttributeSet attrs) {
		super(context, attrs);
		Paint paint = new Paint();
		paint.setColor(Color.GREEN);
		paint.setStrokeWidth(3);
		paint.setStyle(Paint.Style.STROKE);
	}
	
	
	
	@Override
	 protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		setMeasuredDimension(
				MeasureSpec.getSize(widthMeasureSpec),
				MeasureSpec.getSize(heightMeasureSpec));
	 }

	public void drawRect(int rectX, int rectY){

        //drawView.setHaveTouch(false, new Rect(0, 0, 0, 0));
        //drawView.invalidate();

        Rect screenRect = new Rect(
                (int)(rectX - 100),
                (int)(rectY - 100),
                (int)(rectX + 100),
                (int)(rectY + 100));

        getRect(screenRect);

        camPreview.doTouchFocus(targetFocusRect);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
        String currentDateandTime = sdf.format(new Date());
        //cameraPreview.CameraTakePicture(PictureFileName + "_" + currentDateandTime, maxNumPics, lagTime);

        camPreview.CameraTakePicture(touchActivity.PictureFileName + "_" + currentDateandTime, 1, 10);

        if (drawingViewSet) {
            drawingView.setHaveTouch(true, screenRect);
            drawingView.invalidate();

            // Remove the square after some time
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    drawingView.setHaveTouch(false, new Rect(0, 0, 0, 0));
                    drawingView.invalidate();
                }
            }, 1000);
        }

    }

	 @Override
	 public boolean onTouchEvent(MotionEvent event) {
	  if (!listenerSet) {
		  return false;
	  }
	  if(event.getAction() == MotionEvent.ACTION_DOWN){
		  float x = event.getX();
	      float y = event.getY();

          if(touchCount == 0){
              x1 = (int) x;
              y1 = (int) y;
              touchCount = 1;
          }
          else if(touchCount == 1){
              x2 = (int) x;
              y2 = (int) y;
              touchCount = 2;
          }
          else if(touchCount == 2){
              x3 = (int) x;
              y3 = (int) y;
              touchCount = 0;
          }

          isTouched = true;

		  Log.i(TAG, "Touched screen at: (" + x + ", " + y + ")");

	      Rect touchRect = new Rect(
	    		(int)(x - 100), 
	  	        (int)(y - 100), 
	  	        (int)(x + 100), 
	  	        (int)(y + 100));
	      
	      getRect(touchRect);
	      
	      camPreview.doTouchFocus(targetFocusRect);
	      if (drawingViewSet) {
	    	  drawingView.setHaveTouch(true, touchRect);
	    	  drawingView.invalidate();
	    	  
	    	  // Remove the square after some time
	    	  Handler handler = new Handler();
	    	  handler.postDelayed(new Runnable() {
				
				@Override
				public void run() {
					drawingView.setHaveTouch(false, new Rect(0, 0, 0, 0));
					drawingView.invalidate();
				}
			}, 1000);
	      }
	      
	  }
	  return false;
	 }
	
	 /**
	  * set CameraPreview instance for touch focus.
	  * @param camPreview - CameraPreview
	  */
	public void setListener(CameraPreview camPreview) {
		this.camPreview = camPreview;
		listenerSet = true;
	}
	
	/**
	  * set DrawingView instance for touch focus indication.
	  * @param camPreview - DrawingView
	  */
	public void setDrawingView(DrawingView dView) {
		drawingView = dView;
		drawingViewSet = true;
	}

    public void scanImageFiles(ArrayList<String> toBeScanned){

        if(!toBeScanned.isEmpty()) {
            String[] toBeScannedStr = new String[toBeScanned.size()];
            toBeScannedStr = toBeScanned.toArray(toBeScannedStr);

            MediaScannerConnection.scanFile(getContext(), toBeScannedStr,
                    null, new MediaScannerConnection.OnScanCompletedListener() {

                        @Override
                        public void onScanCompleted(String path, Uri uri) {
                            System.out.println("SCAN COMPLETED: " + path);
                            Toast.makeText(getContext(),
                                    "SCAN COMPLETED" + path, Toast.LENGTH_SHORT).show();
                        }
                    });

            toBeScanned.clear();
        }
    }

	public void getRect(Rect screenRect){

		targetFocusRect = new Rect(
				screenRect.left * 2000/this.getWidth() - 1000,  // w = s.l*2000/(tf.l + 1000)
				screenRect.top * 2000/this.getHeight() - 1000,
				screenRect.right * 2000/this.getWidth() - 1000,
				screenRect.bottom * 2000/this.getHeight() - 1000);

	}


	public void test(){



    }


}